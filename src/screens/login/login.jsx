import React, { Component } from "react";
import { TextInput, View, Text, StyleSheet } from "react-native";
import Input from "../../components/basic/input";
import Logo from "../../components/basic/logo";
import translate from "../../stores/translate";
import BasicButton from "../../components/basic/basicButton";
import global from "../../global";
import db from "../../utils/db";

export default class LoginSCR extends Component {
  componentDidMount() {
    this.getUsername();
  }
  getUsername = async () => {
    await db.user_set({
      username: "sada"
    });
    console.log("sss", await db.user_get("sada"));
  };

  submit = () => {
    console.log("submit username");
  };
  render() {
    const { i18n } = translate;
    return (
      <View style={styles.container}>
        <Logo />

        <View style={styles.actionBlock}>
          <Text style={styles.txt}>{i18n.INIT.TXT}</Text>
          <Input
            onSubmitEditing={this.submit}
            returnKeyType={"next"}
            placeholder={i18n.INIT.USERNAME_PLACEHOLDER}
          />
          <BasicButton onPress={this.submit} title={i18n.INIT.ENTER} />
        </View>
        <View style={styles.bottomPart}>
          <Text style={[styles.txt, styles.txtBottom]}>{i18n.INIT.TERMS}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomPart: {
    flex: 0.2
  },
  actionBlock: {
    width: "100%",
    flex: 1,

    alignItems: "center",
    marginTop: "20%"
    // justifyContent: "center"
  },
  txtBottom: {
    textAlign: "left"
  },
  txt: {
    color: global.colors.gray,
    fontWeight: "100",
    marginTop: 10,
    marginBottom: 10,
    textAlign: "center",
    fontSize: 12
  },
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center"
  }
});
