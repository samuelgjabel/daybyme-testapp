/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, StatusBar } from "react-native";
import internal from "./stores/internal";
import Login from "./screens/login/login";
import global from "./global";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Login />
        <StatusBar
          barStyle={internal.data.statusbar}
          backgroundColor={"transparent"}
          translucent
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: global.colors.white,
    paddingTop: 30,
    paddingLeft: 15,
    paddingRight: 15
  }
});
