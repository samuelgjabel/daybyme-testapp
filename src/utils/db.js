import firebase from "react-native-firebase";
import DeviceInfo from "react-native-device-info";
import moment from "moment";
import {
    Platform
} from "react-native";

class db {
    db = firebase.firestore();
    constructor() {
        this.users = this.db.collection("users");
        this.devices = this.db.collection("devices");
    }
    /**
     * Get user by username
     * @param {String} username
     */
    user_get = async username => {
        let ret = {
            success: false,
            data: null,
            error: null
        };
        if (!username) {
            ret.error = "no username provided!";
            return ret;
        }
        try {
            const querySnapshot = await this.users
                .where("username", "==", username)
                .get();
            let i;


            let data = {};
            for (i = 0; i < querySnapshot.docs.length; i++) {
                data = querySnapshot.docs[i].data();
                break;
            }
            ret.success = true;
            ret.data = data;
        } catch (err) {
            ret.error = err;
        }

        return ret;
    };
    /**
     * Get user by username
     * @param {Object} params
     * @param {String} params.username
     * @param {boolean} update //if user is updating or createing new one.
     */
    user_set = async (params, update) => {
        let ret = {
            success: false,
            data: null,
            error: null
        };

        const newParams = {
            ...params,
            updated: moment().unix(),
        };

        if (!update) {
            newParams.created = moment().unix();
        }
        console.log(params.username, JSON.stringify(newParams))
        try {
            await this.users.doc(params.username).set(newParams);
            ret.success = true;
        } catch (err) {
            ret.error = err;
        }

        return ret;
    };
}

export default new db();