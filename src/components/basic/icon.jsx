import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
//GLOBAL
import Icon from "react-native-vector-icons/dist/Feather";
import Icon2 from "react-native-vector-icons/dist/MaterialIcons";
import Icon3 from "react-native-vector-icons/dist/FontAwesome5";
import global from "../../global";
export default class Iconky extends Component {
  constructor(props) {
    super(props);
    // this.state = { };
  }
  render() {
    if (this.props.material) {
      return (
        <Icon2
          name={this.props.name || "home"}
          size={this.props.size || 20}
          color={this.props.color || global.colors.white}
        />
      );
    }
    if (this.props.awesome) {
      return (
        <Icon3
          name={this.props.name || "home"}
          size={this.props.size || 20}
          color={this.props.color || global.colors.white}
        />
      );
    }
    return (
      <Icon
        name={this.props.name || "home"}
        size={this.props.size || 20}
        color={this.props.color || global.colors.white}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink"
  }
});
