import React, { Component } from "react";
import { TextInput, View, Text, StyleSheet } from "react-native";
import global from "../../global";

interface IInput {
  placeholder: string;
  value: string;
}
export default class Input extends Component<IInput> {
  render() {
    const { returnKeyType } = this.props;
    return (
      <View style={styles.container}>
        <TextInput
          selectionColor={global.colors.primary}
          returnKeyType={returnKeyType}
          {...this.props}
          style={styles.input}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    flex: 1
  },
  container: {
    width: "100%",
    height: 50,
    borderRadius: 10,
    paddingRight: 10,
    paddingLeft: 10,
    // paddinngLeft: 10,
    // paddingRight: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: global.colors.primary
  }
});
