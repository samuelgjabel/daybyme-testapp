import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import global from "../../global";
import Touchable from "../basic/touchable";
import Loading from "./loading";
export default ({ className, title, onPress, loading }) => {
  return (
    <Touchable onPress={onPress} style={[styles.container, className]}>
      {!loading ? (
        <Text style={styles.txt}>{title}</Text>
      ) : (
        <Loading color={global.colors.white} size={"small"} />
      )}
    </Touchable>
  );
};

const styles = StyleSheet.create({
  txt: {
    color: global.colors.white
  },
  input: {
    flex: 1
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    width: "100%",
    height: 50,
    borderRadius: 10,
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: global.colors.primary
  }
});
