import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import global from "../../global";
interface IImage {
  style: string;
}
const LOGO = require("../../assets/images/logo.png");
export default class Image_ extends Component<IImage> {
  render() {
    const { returnKeyType } = this.props;
    return (
      <View style={styles.container}>
        <Image style={styles.img} source={LOGO} />
        <Text style={styles.txt}>{global.appName}</Text>
        <View style={styles.designLine} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  designLine: {
    width: "70%",
    backgroundColor: global.colors.secondary,
    height: StyleSheet.hairlineWidth,
    marginTop: 25,
    opacity: 0.4
  },
  txt: {
    marginTop: 20,
    fontSize: 16,
    color: global.colors.black,
    letterSpacing: 20,
    fontWeight: "100"
  },
  img: {
    height: 70,
    width: 70
  },
  container: {
    width: "100%",
    height: "30%",
    alignItems: "center",
    justifyContent: "center"
  }
});
