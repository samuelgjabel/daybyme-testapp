import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Platform,View, TouchableNativeFeedback, TouchableWithoutFeedback } from 'react-native';
//GLOBAL
import global from '../../global'
export default class Default extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pressed:false
     };
  }
  onprss = () => {
      if (this.props.onPress) {
          this.props.onPress()
      }
  }
  pressIn = () => {
    if (this.props.inPress) {
      this.props.inPress()
    }
    this.setState({
      pressed:true
    })
  }
  pressout =  () => {
    if (this.props.outPress) {
      this.props.outPress()
    }
    this.setState({
      pressed:false
    })
  }
  render() {
    if (this.props.pressColor) {
      return (
        <TouchableWithoutFeedback {...this.props} onPressIn={this.pressIn} onPressOut={this.pressout} disabled={this.props.disabled} onPress={this.onprss} >
          <View activeOpacity={this.props.activeOpacity || 0.2}  style={[this.props.style, this.state.pressed ? {backgroundColor:this.props.pressColor} : null]}>
            {this.props.children}
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      if (Platform.OS === 'ios') {
        return (
          <TouchableOpacity {...this.props}   activeOpacity={this.props.activeOpacity || 0.2} disabled={this.props.disabled} onPress={this.onprss} style={this.props.style}>
            {this.props.children}
          </TouchableOpacity>
        );
      } else {
        if (this.props.androidOpacity) {
          return (
            <TouchableOpacity {...this.props}   activeOpacity={this.props.activeOpacity || 0.2} disabled={this.props.disabled} onPress={this.onprss} style={this.props.style}>
              {this.props.children}
            </TouchableOpacity>
          );
        } else {
          return (
            <TouchableNativeFeedback
            {...this.props}
            disabled={this.props.disabled}
            onPress={this.onprss}
    
            background={this.props.rounded ? TouchableNativeFeedback.Ripple(global.colors.secondary, true) : TouchableNativeFeedback.SelectableBackground()}
            >
            <View style={this.props.style}>
              {this.props.children}
            </View>
          </TouchableNativeFeedback>
          );
        }
    }

    }

  }
}

const styles = StyleSheet.create({

  container: {
    flex:1,
    backgroundColor:'pink'
  },

});
