import React, { Component } from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";
//GLOBAL
import global from "../../global";
export default ({ style, size, color }) => {
  return (
    <View style={[styles.container, style]}>
      <ActivityIndicator
        size={size || "large"}
        color={color ? color : global.colors.secondary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex:1,
    // backgroundColor:'pink',
    justifyContent: "center",
    alignItems: "center"
  }
});
