module.exports = {
    colors: {
        primary: '#A06CD5',
        secondary: '#4e3765',
        danger: 'red',
        ok: 'green',
        white: 'white',
        black: 'black',
        gray: '#999'
    },
    appName: 'listify'
}