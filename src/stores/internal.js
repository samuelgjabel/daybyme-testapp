import {
    decorate,
    observable,
    action,

} from 'mobx'

/**
 * Internal handing class also connected with mobx for observable, computed data, but sometimes it can load just normal data.
 * Storing just data from mobile device as height, width of sceen or config / fill / load global components ++
 * Also iam using mobx without decorator (@observe, @etc...) becouse of newer vesions of react doesnt support it, or at least its hard to figure out the best config for it.
 */
class Internal {
    data = {
        statusbar: 'dark-content',
        select: {
            loaded: false,
            data: [],
            title: '',
            open: false,
            selected: 0,
            id: '',
            search: [],
            itemTitle: '',
            subItemTitle: '',
            onPress: () => '',
        },
        /**
         * screen 0-init,1-home,2-add item 
         * There can be used another library like react-navigation, but I think its not neceserry for app with size like this (2-3 screens).
         */
        screen: 0
    }


    openSelect = (data, biGdata) => {
        let bigData = false;
        if (biGdata) {
            bigData = true;
        }
        if (bigData) {
            setTimeout(() => {
                this.data.select.loaded = true;
            }, 100)
        } else {
            this.data.select.loaded = true;
        }

        this.data.select.data = data.data;
        this.data.select.title = data.title;
        this.data.select.selected = data.selected;
        this.data.select.id = data.id;
        this.data.select.subItemTitle = data.subItemTitle;
        this.data.select.itemTitle = data.itemTitle;
        this.data.select.search = data.search;
        this.data.select.onPress = data.onPress;
        this.data.select.open = true;


    }

    closeSelect = () => {
        setTimeout(() => {
            this.data.select.loaded = false;
        }, 300)
        this.data.select.open = false;
    }

}
decorate(Internal, {
    data: observable,
    openSelect: action,
    closeSelect: action
})


export default new Internal()