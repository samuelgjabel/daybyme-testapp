import {
    decorate,
    observable,

} from 'mobx'
import en from '../assets/i18n/en'

/**
 * Simple language store becouse app dont need another langnuages ---- but for future purposes... :)
 * With these store you can globaly change the app langauge without using external dependency, just mobx observable.
 * Also iam using this class becouse of easily adding, editinng langauges via server ===  Fetch translations from server.
 */
class Translate {

    i18n = en;
}
decorate(Translate, {
    i18n: observable
})


export default new Translate()