# Demo test app of simple list

## For run this applicatio just follow steps here:

`https://facebook.github.io/react-native/docs/getting-started.html`

hint:
Also you need to install `CocoaPods` https://cocoapods.org/

1. Open `root/ios` in terminal & run `pod install`
2. Open `root` in terminal & run `react-native run-ios` or `react-native run-android`
3. Enjoy the app :)

#### Intallation / running problems:

If you have problem with bundling ios app (cocoa problem with xcode 10), updating of cocoapods should fix the issue. `sudo gem install cocoapods --pre` 1.6.0 beta1.
